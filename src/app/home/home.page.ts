import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {BarcodeScannerOptions, BarcodeScanner} from "@ionic-native/barcode-scanner/ngx";

// import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
// import {BarcodeScanner,BarcodeScannerOriginal, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

import * as firebase from 'firebase';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public mostrar = true;
  public creditoActual: string = "20";
  public mensajeAlerta: string = "Sin mensaje actual";
  public usuarioActual: string = "";

  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;

  constructor(private router: Router, private barcodeScanner: BarcodeScanner) {

    this.usuarioActual = localStorage.getItem('usuario');

    this.freno(100).then(() => {
      this.mostrar = false;
    });
  }

  ngOnInit() {
    //this.iniciarlizarAudios();
  }


  cargarCredito() {

    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        alert("Barcode data " + JSON.stringify(barcodeData));
        this.scannedData = barcodeData;
      })
      .catch(err => {
        console.log("Error", err);
      });
  }

  guardarCredito(creditoRecibido: string) {

    var usuariosRef = firebase.database().ref().child("creditos");
    usuariosRef.push({ usuario: this.usuarioActual, credito: creditoRecibido }).then(() => {

    });
  }


  borrarCredito() {

    // var usuariosRef = firebase.database().ref().child("creditos");
    // usuariosRef.push({ usuario: this.usuarioActual, credito: null }).then(() => {

    // });
  }

  salir() {


    this.router.navigate(['ingreso']);
  }

  async freno(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log("fired"));
  }

}